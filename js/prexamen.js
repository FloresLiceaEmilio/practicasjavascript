const btnTabla = document.getElementById('btnTabla');
btnTabla.addEventListener('click', function(){
    var select = document.getElementById('cmbTablas');
    var tabla = document.getElementById('tabla');

    tabla.innerHTML = '';
    var numeroTabla = parseInt(select.value);
    var arreglo = [];

    //generar arreglo
    for(var i=1; i <= 10; i++){
        arreglo.push(numeroTabla * i);
    }

    //crear tabla con resultados
    var tablaHTML = '<table>';
    for(var j = 0; j < arreglo.length; j++){
        tablaHTML += '<tr>';
        tablaHTML += '<td><img src="/img/' + select.value + '.png" alt="imagen ' + select.value + '"></td>';
        tablaHTML += '<td><img src="/img/x.png" alt="x"></td>';
        tablaHTML += '<td><img src="/img/' + (j+1) + '.png" alt="imagen' + (j+1) + '"></td>';
        tablaHTML += '<td><img src="/img/igual.png" alt="="></td>';
        var digitos = arreglo[j].toString().split('');
        for (var k = 0; k < digitos.length; k++) {
            tablaHTML += '<td><img src="/img/' + digitos[k] + '.png" alt="' + digitos[k] + '"></td>';
        }
        tablaHTML += '</tr>';
    }
    tablaHTML += '</table>';
    tabla.innerHTML = tablaHTML;
});

