//declaracion de arreglos
var arreglo = [19,20,100,2,6,3,3,9];

//mostrar los elementos de arreglos

function mostrarArray(){

    for( con=0; con<arreglo.length; ++con){

        console.log(con + ":" + arreglo[con]);


    }

}

//funcion que regresa el promedio de los elementos del arreglo

function promedio(){
    let pro = 0;

    for(con=0; con<arreglo.length; ++con){
        pro+=arreglo[con];
    }
    pro = pro/arreglo.length;
    return pro;
}


//realizar una funcion que regrese un valor entero
//que represente la cantidad de numeros pares en el arreglo
function numPares(){
    let par = 0;
    let impar = 0;
    for(con=0; con<arreglo.length; ++con){

        if(arreglo[con] % 2 == 0){
            par++;
        }else{
            impar++;
        }

    }

    par = (par*100) / arreglo.length;
    impar = (impar*100) / arreglo.length;

    return par, impar;
}
//realizar una funcion que te regrese el valor mayor , contenido en array
function mayor(){

    mayores = 0;
    indiceM = 0;

    for(con=0; con<arreglo.length; ++con){
        if(arreglo[con]>=mayores){
            mayores = arreglo[con];
            indiceM = con;
        }
        
    }

    menores = mayores;
    indicem = 0;

    for(con=0; con<arreglo.length; ++con){
        if(arreglo[con]<=menores){
            menores = arreglo[con];
            indicem = con;
        }
    }

    return mayores, menores, indiceM, indicem;
}
mayor();
numPares();
mostrarArray();
const prom = promedio();
const pares = numPares();
const mayo = mayor();

//diseñe una funcion que recibe un valor numerico que indica la cantidad de elementos del arreglo
//el arreglo debera de llenarse con valores aleatorios en el rango del 0 al 1000
//posteriormente mostrara el arreglo

document.addEventListener('DOMContentLoaded', function() {
    let generar = document.getElementById('btnGenerar');
    let promedioLabel = document.getElementById('comprobante');
    let parLabel = document.getElementById('parLabel');
    let imparLabel = document.getElementById('imparLabel');
    let mayorlabel = document.getElementById('mayor');
    let menorlabel = document.getElementById('menor');
    let simetricoLabel = document.getElementById('simetrico');
  
    generar.addEventListener('click', function() {
      let tamaño = document.getElementById('tamanoArea').value;
      const cmbAleatorios = document.getElementById('cmbNumeros');
      var arreAle = [];
      
  
      cmbAleatorios.innerHTML = '';
  
      for (con = 0; con < tamaño; con++) {
        arreAle[con] = Math.floor(Math.random() * 1000);
        let option = document.createElement('option');
        option.value = arreAle[con];
        option.innerHTML = arreAle[con];
        cmbAleatorios.appendChild(option);
      }
  
      const promedio = calcularPromedio(arreAle);
      const { pares, impares } = numPares(arreAle);
      const esSimetrico = pares === impares;
      const { mayor , menor, indiceMayor, indiceMenor } = encontrarMayorMenor(arreAle);
      
        promedioLabel.innerHTML = "El promedio es: " + promedio.toFixed(2);
        parLabel.innerHTML = "pares: " + pares.toFixed(2) + "%";
        imparLabel.innerHTML = "impares: " + impares.toFixed(2) + "%";
        mayorlabel.innerHTML = "Mayor: " + mayor + ", Indice: " + indiceMayor;
        menorlabel.innerHTML = "Menor: " + menor + ", Indice: " + indiceMenor;
        simetricoLabel.innerHTML = "simétrico: " + (esSimetrico ? "Sí" : "No");

    });
  
    function calcularPromedio(arreAle) {
      let suma = 0;
      for (let i = 0; i < arreAle.length; i++) {
        suma += arreAle[i];
      }
      return suma / arreAle.length;
    }

    function numPares(arreAle){
        let par = 0;
        let impar = 0;
        for(con=0; con<arreAle.length; ++con){
    
            if(arreAle[con] % 2 == 0){
                par++;
            }else{
                impar++;
            }
    
        }
    
        par = (par*100) / arreAle.length;
        impar = (impar*100) / arreAle.length;
        return { pares: par, impares: impar };

    }

    function encontrarMayorMenor(arreAle) {
        let mayor = arreAle[0];
        let menor = arreAle[0];
        let indiceMayor = 0;
        let indiceMenor = 0;

        for (con = 1; con < arreAle.length; con++) {
            if (arreAle[con] > mayor) {
                mayor = arreAle[con];
                indiceMayor = con;
            }
            if (arreAle[con] < menor) {
                menor = arreAle[con];
                indiceMenor = con;
            }
        }
        return { mayor: mayor, menor: menor, indiceMayor: indiceMayor, indiceMenor: indiceMenor };

    }
  });



