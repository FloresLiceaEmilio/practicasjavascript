//boton calcular IMC
const btnCalcularIMC = document.getElementById('btnCalcularImc');
btnCalcularIMC.addEventListener('click', function(){
    let alturaM = document.getElementById('txtAlturaM').value;
    let pesoKg = document.getElementById('txtPesoKg').value;
    let Desc = document.getElementById('descripcion');
    let generos = document.querySelector('input[name="sexo"]:checked').value;
    let Edad = document.getElementById('txtEdad').value;
    let Calorias;


    //hacer calculos
    const IMC = pesoKg / (alturaM * alturaM) ;

    //mostrar datos
    document.getElementById('txtIMC').value = IMC.toFixed(2);

    if(generos == 'm'){
        cargaImagen(IMC);
        if (Edad >= 10 && Edad < 18){
            Calorias = 17.686 * pesoKg + 658.2;
        } else if (Edad >= 18 && Edad < 30){
            Calorias = 15.057 * pesoKg + 692.2;
        } else if (Edad >= 30 && Edad < 60){
            Calorias = 11.472 * pesoKg + 873.1;
        } else if (Edad >= 60){
            Calorias = 11.711 * pesoKg + 587.1;
        }
        document.getElementById('txtCalorias').value = Calorias.toFixed(2);
    }else{
        cargaImagenF(IMC); 
        if (Edad >= 10 && Edad < 18) {
            Calorias = 13.384 * pesoKgo + 692.6;
        } else if (Edad >= 18 && Edad < 30){
            Calorias = 14.818 * pesoKg + 486.6;
        } else if (Edad >= 30 && Edad < 60){
            Calorias = 8.126 * pesoKg + 845.6;
        } else if (Edad >= 60){
            Calorias = 9.082 * pesoKg + 658.5;
        }
        document.getElementById('txtCalorias').value = Calorias.toFixed(2);
    }
    


    function cargaImagen(IMC){
        let img = document.getElementById('imagen');
            if(IMC<18.5){
               img.src = "/img/01H.png"; 
               Desc.innerHTML = "Esta por debajo de lo saludable"
            } 

            else if(IMC < 24.9){
                img.src="/img/02H.png";
                Desc.innerHTML = "Saludable"
            }

            else if(IMC < 29.9) {
                img.src="/img/03H.png";
                Desc.innerHTML = "Tienes Sobrepeso"
            }
        
            else if(IMC < 34.9) {
                img.src="/img/04H.png";
                Desc.innerHTML = "Tienes obesidad, entra en dieta"
            }

            else if(IMC < 39.9) {
                img.src="/img/05H.png";
                Desc.innerHTML = "Tienes obesidad sevra, porfavor ve a un nutriologo"
            }

            else if(IMC > 40) {
                img.src="/img/06H.png";
                Desc.innerHTML = "Tienes obesidad morbida, ocupas ayuda de inmediato"
            }
            
    }
    function cargaImagenF(IMC){
        let img = document.getElementById('imagen');
        if(IMC<18.5) {
            img.src = "/img/01M.png";
            Desc.innerHTML = "Esta por debajo de lo saludable"
        }

            else if(IMC < 24.9) {
                img.src = "/img/02M.png";
                Desc.innerHTML = "Saludable"
            }

            else if(IMC < 29.9) {
                img.src = "/img/03M.png";
                Desc.innerHTML = "Tienes Sobrepeso"
            }
        
            else if(IMC < 34.9) {
                img.src = "/img/04M.png";
                Desc.innerHTML = "Tienes obesidad, entra en dieta"
            }

            else if(IMC < 39.9) {
                img.src = "/img/05M.png";
                Desc.innerHTML = "Tienes obesidad sevra, porfavor ve a un nutriologo"
            }

            else if(IMC > 40) {
                img.src = "/img/06M.png";
                Desc.innerHTML = "Tienes obesidad morbida, ocupas ayuda de inmediato"
            }
            
    }
    
});